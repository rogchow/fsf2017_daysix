// for client side we use IIFE to execute the application
(function() {
    //Everything inside this function is our application
    // create an instance of Angular application
    // 1st param: app's name , this is the apps dependencies
    var FirstApp = angular.module("FirstApp",[]);

    // This is the function of the FirstCtrl
    var FirstCtrl = function(){

        // Hold a reference of this cntroller so that when"this" changes we are 
        // still referencing the controller
        //var firstCtrl = this;