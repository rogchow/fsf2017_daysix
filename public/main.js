//load express and path
var path = require ("path");
var express = require ("express");

//create an instance of express app
var app = express();

//configure the port
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_port) || 3000);

app.use(express.static(path.join(__dirname,"public")));
app.use("/libs",express.static(path.join(__dirname,"bower_components")));

//start of app
console.log(parseInt(app.get("port")));
app.listen(app.get("port"),function() {
    console.log("Application is listening on port" + app.get("port"))
    console.log("yaaaah..")
});